# Структура папки
**ВНИМАНИЕ!** Несанкционированное изменение, или удаление файлов, находящихся в данном каталоге (за исключением случаев, описанных особо), **может привести к полной неработоспособности системы**.
Если у Вас нет опыта, ничего тут не трогайте.
В любом случае, перед любыми изменениями _сделайте резервную копию системы_.

## cache
Данный каталог содержит временные файлы и их удаление никак не влияет на работоспособность системы. Каталог при желании можно полностью очистить, все файлы кэша пересоздадутся автоматически.

## config
В данном каталоге и его подкаталогах содержатся различные конфигурационные файлы,
которые считываются при загрузке системы и непосредственно влияют на работу.
- **custom** - конфигурационные файлы, которые создаются при установке, или работе системы.
Непосредственно влияют на поведение системы. Изменение, или удаление файлов (за исключением случаев описанных в руководстве) могут привести к непредсказуемому поведению приложения, или полной неработоспособности.
- **packages** - конфигурационные файлы, которые создаются при инсталляции пакетов
и считываются автоматически при загрузке системы. _Изменение, или удаление файлов в данном каталоге категорически запрещено!_

## logs
Файлы системных журналов. Создаются автоматически по мере работы, или по результату определенных событий.
Все содержимое папки можно удалять, без влияния на работоспособность системы.
_Файлы с логами ошибок полезно перед удалением просмотреть и по возможности исправить код, ошибка которого была запротоколирована._

## vendor
Каталог создается после установки с помощью Composer. Тут содержатся различные библиотеки-зависимости.
_Изменение, или удаление файлов в данном каталоге категорически запрещено!_
