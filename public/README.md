# Структура папки
**ВНИМАНИЕ!** Несанкционированное изменение, или удаление файлов, находящихся в данном каталоге (за исключением случаев, описанных особо), **может привести к непредсказуемому поведению системы, или потере данных**.
Если у Вас нет опыта, ничего тут не трогайте.
В любом случае, перед любыми изменениями _сделайте резервную копию системы_.
